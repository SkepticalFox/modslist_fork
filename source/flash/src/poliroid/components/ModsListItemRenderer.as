﻿package poliroid.components
{
    import flash.display.*;
    import flash.utils.ByteArray;
    import flash.events.*;
    import flash.text.*;
    import flash.filters.*;
    import scaleform.clik.constants.InvalidationType;
    import net.wg.gui.components.controls.SoundListItemRenderer;
    import poliroid.utils.Base64;

    public class ModsListItemRenderer extends SoundListItemRenderer
    {
        [Embed(source="../../../res/modsListApi/button_default.jpg")]
        private static var imageButtonDefault:Class;

        [Embed(source="../../../res/modsListApi/button_hover.jpg")]
        private static var imageButtonHover:Class;

        [Embed(source="../../../res/modsListApi/button_disabled.jpg")]
        private static var imageButtonDisabled:Class;

        [Embed(source="../../../res/modsListApi/alert_icon.png")]
        private static var imageAlertIcon:Class;

        public var tf_modName:TextField;
        public var modIcon:MovieClip;
        public var id:String;
        public var descr:String;
        public var iconStr:String;
        public var icoLoader:Loader;
        private var _hovered:Boolean = false;
        private var _enabled:Boolean = true;
        private var _images:Object = new Object();

        //private var popupCursor:Bitmap = null;

        public function ModsListItemRenderer()
        {
            super();
        }

        override public function setData(newDat:Object):void
        {
            if (newDat == null) {
                return;
            }
            this.data = newDat;
            invalidateData();
        }

        override protected function configUI(): void {
            super.configUI();

            width = 280;
            height = 70;

            _images.button_default = new imageButtonDefault() as Bitmap;
            _images.button_default.visible = false;
            addChild(_images.button_default);

            _images.button_hover = new imageButtonHover() as Bitmap;
            _images.button_hover.visible = false;
            addChild(this._images.button_hover);

            _images.button_disabled = new imageButtonDisabled() as Bitmap;
            _images.button_disabled.visible = false;
            addChild(this._images.button_disabled);

            _images.alert_icon = new imageAlertIcon() as Bitmap;
            _images.alert_icon.visible = false;
            addChild(_images.alert_icon);

            tf_modName = new TextField();
            tf_modName.x = 70;
            tf_modName.y = 20;
            tf_modName.width = 190;
            tf_modName.height = 25;
            addChild(tf_modName);

            modIcon = new MovieClip();
            modIcon.x = 15;
            modIcon.y = 10;
            modIcon.width = 50;
            modIcon.height = 50;
            addChild(modIcon);

            if (data) {
                setup();
            }
        }

        override protected function handleMouseRollOver(event:MouseEvent):void
        {
            super.handleMouseRollOver(event);
            _hovered = true;
            drawBG();
            App.toolTipMgr.show(descr);
        }

        override protected function handleMouseRollOut(event:MouseEvent):void
        {
            super.handleMouseRollOut(event);
            _hovered = false;
            drawBG();
            App.toolTipMgr.hide();
        }

        override protected function draw():void
        {
            if (isInvalid(InvalidationType.DATA)) {
                setup();
            }
            super.draw();
            if (!data) {
                visible = false;
            }
            _images.alert_icon.visible = data.alert;
        }

        private function setup():void
        {
            if (this.data) {
                id = data.id;
                descr = data.description;
                iconStr = data.icon;
                _enabled = data.enabled;

                if (_enabled) {
                    tf_modName.defaultTextFormat = new TextFormat("$FieldFont", 16, 0xDDDDD0, true);
                    tf_modName.alpha = 1;
                } else {
                    tf_modName.defaultTextFormat = new TextFormat("$FieldFont", 16, 0xCCCCCC, true);
                    tf_modName.alpha = 0.9;
                }

                tf_modName.text = data.name;

                setModIcon();
                drawBG();
            }
        }

        private function drawBGBase(elem:Bitmap):void
        {
            _images.button_disabled.visible = false;
            _images.button_hover.visible = false;
            _images.button_default.visible = false;
            elem.visible = true;
        }

        private function drawBG():void
        {
            if (_enabled == false) {
                drawBGBase(_images.button_disabled);
                return;
            }
            if (_hovered == true) {
                drawBGBase(_images.button_hover);
            } else {
                drawBGBase(_images.button_default);
            }
        }

        private function setModIcon():void
        {
            icoLoader = new Loader();
            icoLoader.loadBytes(Base64.decodeToByteArray(iconStr));
            icoLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, icoLoadComplete);
        }

        private function icoLoadComplete(e:Event):void
        {
            var bmp:BitmapData = new BitmapData(50, 50, true, 0x0);
            bmp.draw(icoLoader);

            modIcon.addChild(new Bitmap(bmp));
            modIcon.height = modIcon.width = 50;

            var shadow:DropShadowFilter = new DropShadowFilter(0, 0, 0, 0.7, 4, 4, 3, 8);

            if (!_enabled) {
                var cc:Number = 1/4;
                var greyscale:ColorMatrixFilter = new ColorMatrixFilter([cc, cc, cc, 0, 0, cc, cc, cc, 0, 0, cc, cc, cc, 0, 0, 0, 0, 0, 1, 0]);
                modIcon.filters = [greyscale, shadow];
                modIcon.alpha = 0.8;
            } else {
                modIcon.filters = [shadow];
                modIcon.alpha = 1;
            }
        }
    }
}
