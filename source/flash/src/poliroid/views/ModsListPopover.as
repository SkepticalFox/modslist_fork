package poliroid.views
{
    import net.wg.infrastructure.base.SmartPopOverView;

    import poliroid.components.ModsListItemRenderer;

    import scaleform.clik.controls.ScrollingList;
    import scaleform.clik.data.DataProvider;
    import scaleform.clik.events.ListEvent;
    import flash.text.*;

    public class ModsListPopover extends SmartPopOverView
    {
        public var getModsListS:Function;
        public var callModS:Function;
        public var clearAlertS:Function;

        private var _data:Array;
        private var _list:ScrollingList;
        private var _headerText:TextField;
        private var titleText:String = "Список модификаций";

        public function ModsListPopover()
        {
            super();
        }

        override protected function onPopulate():void
        {
            super.onPopulate();
            width = 280;
            height = 65;

            _headerText = new TextField();
            _headerText.x = 15;
            _headerText.y = 15;
            _headerText.width = 250;
            _headerText.height = 30;
            _headerText.selectable = false;
            _headerText.defaultTextFormat = new TextFormat("$FieldFont", 18, 0xDDDDD0, true);
            _headerText.text = titleText;
            addChild(_headerText);

            _list = addChild(App.utils.classFactory.getComponent("ScrollingList", ScrollingList, {
                x: 0,
                y: 55,
                width: 280,
                margin: 0,
                itemRenderer: ModsListItemRenderer,
                itemRendererInstanceName: "",
                rowHeight: 70,
                rowCount: 4,
                enabled: true,
                visible: true,
                scrollBar: "",
                wrapping: "normal"
            })) as ScrollingList;

            getModsListS();

            _list.addEventListener(ListEvent.ITEM_CLICK, handleItemClick);
        }

        private function handleItemClick(event:ListEvent):void
        {
            var target:ModsListItemRenderer = event.itemRenderer as ModsListItemRenderer;
            callModS(target.id);
            clearAlertS(target.id);
            App.popoverMgr.hide();
        }

        public function as_setTitleText(new_titleText:String):void
        {
            titleText = new_titleText;
            if (_headerText != null) {
                _headerText.text = titleText;
            }
        }

        public function as_setData(new_data:Array):void
        {
            if (new_data.length < 1) {
                new_data = [{
                    id: "none",
                    name: " ",
                    description: " ",
                    icon: " ",
                    enabled: false,
                    alert: false
                }];
            }
            _data = new_data;
            height = _list.y + _data.length * 70 + 10;

            _list.rowCount = _data.length;
            _list.dataProvider = new DataProvider(_data);
            _list.validateNow();
        }
    }
}
